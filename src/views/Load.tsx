import Box from '@mui/material/Box';
import Button from '@mui/material/Button';
import FormControl from '@mui/material/FormControl';
import InputLabel from '@mui/material/InputLabel';
import MenuItem from '@mui/material/MenuItem';
import Select from '@mui/material/Select';
import React, { useEffect, useState } from 'react';
import { MyTextInput } from '../common-front/components/MyTextInput';
import { IAddressSet } from '../interfaces/apiInterfaces';
import { useApp } from '../services/app.service';

export const Load = () => {
  const { notify, apiService } = useApp();
  const [collectionName, setCollectionName] = useState<string>('');
  const [description, setDescription] = useState<string>('');
  const [authors, setAuthors] = useState<string>('');
  const [addressesSetId, setAddressesSetId] = useState<string>('1');
  const [addressesSets, setAddressesSets] = useState<IAddressSet[] | null>(
    null,
  );
  const [assetsName, setAssetsName] = useState<string>('');
  const [fileList, setFileList] = useState<FileList | null>(null);
  const [format, setFormat] = useState<'svg' | 'png'>('svg');
  const handleCollectionNameChange = (e: any) => {
    setCollectionName(e.target.value);
  };
  const handleAssetsNameChange = (e: any) => {
    setAssetsName(e.target.value);
  };
  const handleDescriptionChange = (e: any) => {
    setDescription(e.target.value);
  };
  const handleAuthorsChange = (e: any) => {
    setAuthors(e.target.value);
  };
  const handleSelectAddressesSetId = (e: any) => {
    setAddressesSetId(e.target.value);
  };
  const handleSelectFormat = (e: any) => {
    setFormat(e.target.value);
  };
  const handleFileSelect = (e: any) => {
    setFileList(e.target.files);
  };
  const getAddressesSets = async () => {
    const sets = await apiService.getAddressesSets();
    setAddressesSets(sets);
  };
  useEffect(() => {
    getAddressesSets();
  });
  const handleUploadFiles = async (e: any) => {
    e.preventDefault();
    if (fileList === null) return;
    // console.log(fileList);
    const data = new FormData();
    const fl = fileList[0];
    data.append('collectionName', collectionName);
    data.append('assetsName', assetsName);
    data.append('description', description);
    data.append('authors', authors);
    data.append('file', fl);
    data.append('addressesSetId', addressesSetId);
    data.append('format', format);
    console.log(data);
    try {
      const uploadResult = await apiService.createNewCollectionWithAssets(data);
      notify({
        msg: `Files uploaded! ${uploadResult.numberOfFiles}
        All files valid: ${uploadResult.isAllFilesValid}
        First failed file: ${uploadResult.firstNotValidEntryId}
        Contract deployed: ${uploadResult.contractAddress}`,
      });
    } catch (e) {
      notify({ msg: 'Can not upload files.' });
      console.error(e);
    }
  };
  return (
    <div id='LoadView' className='Content'>
      <Box
        component='form'
        sx={{
          '& .MuiTextField-root': { m: 1, width: '25ch' },
        }}
        noValidate
        autoComplete='off'
      >
        <MyTextInput
          type='text'
          name='collectionName'
          placeholder='Enter collection name'
          onChange={handleCollectionNameChange}
          required={true}
        />
        <br />
        <MyTextInput
          name='description'
          onChange={handleDescriptionChange}
          placeholder='Description for collection'
          multiline
          required
        />
        <br />
        <MyTextInput
          type='text'
          name='authors'
          placeholder='Authors, delimited by comma'
          onChange={handleAuthorsChange}
          required={true}
        />
        <br />
        <MyTextInput
          type='text'
          name='assetsName'
          placeholder='Enter assets pack name'
          onChange={handleAssetsNameChange}
          required={true}
        />
        <br />
        <FormControl fullWidth>
          <InputLabel id='addressSetSelect'>Select addresses set</InputLabel>
          <Select
            labelId='addressSetSelect'
            value={1}
            label='Select addresses set'
            onChange={handleSelectAddressesSetId}
          >
            {addressesSets ? (
              addressesSets.map((addressSet) => {
                const id = addressSet.id;
                return (
                  <MenuItem
                    key={id}
                    value={id}
                  >{`${id}_${addressSet.setName}_${addressSet.admin}`}</MenuItem>
                );
              })
            ) : (
              <MenuItem key='1' value='1'>
                1
              </MenuItem>
            )}
          </Select>
        </FormControl>
        <br />
        <FormControl fullWidth>
          <InputLabel id='formatSelect'>Select format</InputLabel>
          <Select
            labelId='formatSelect'
            value='svg'
            label='Select format'
            onChange={handleSelectFormat}
          >
            {
              <MenuItem key='svg' value='svg'>
                SVG
              </MenuItem>
            }
          </Select>
        </FormControl>
        <br />
        <section title='Select assets'>
          <h3>Please, load an archive with asset's</h3>
          <input
            type='file'
            title='files'
            name='files'
            multiple={false}
            onChange={handleFileSelect}
          />
        </section>
        <br />
        <Button variant='contained' onClick={handleUploadFiles}>
          send
        </Button>
      </Box>
    </div>
  );
};
