import Paper from '@mui/material/Paper';
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import { useCallback, useEffect, useReducer, useState } from 'react';
import { shortenTezosAddress } from '../common-front/common/utils';
import { GridContent } from '../common-front/components/GridContent';
import { MyTextInput } from '../common-front/components/MyTextInput';
import { IAddressSet } from '../interfaces/apiInterfaces';
import { useApp } from '../services/app.service';

export const AddAddressSetForm = (props: any) => {
  return (
    <form action='POST'>
      <MyTextInput
        type='text'
        name='setName'
        placeholder='Enter addresses set name'
        onChange={props.handleChange}
        required={true}
      />
      <br />
      <MyTextInput
        type='text'
        name='mintKey'
        placeholder='Enter minter public key'
        onChange={props.handleChange}
        required={true}
      />
      <br />
      <MyTextInput
        type='text'
        name='minter'
        placeholder='Enter minter address'
        onChange={props.handleChange}
        required={true}
      />
      <br />
      <MyTextInput
        type='text'
        name='admin'
        placeholder='Enter admin address'
        onChange={props.handleChange}
        required={true}
      />
      <br />
      <MyTextInput
        type='text'
        name='artist'
        placeholder='Enter artist address'
        onChange={props.handleChange}
        required={true}
      />
      <br />
      <button onClick={props.handleFormSend}>send</button>
    </form>
  );
};
export const AddressSetsList = (props: { addressSets: IAddressSet[] }) => {
  return (
    <TableContainer component={Paper} color='primary.main'>
      <Table sx={{ minWidth: 650 }} aria-label='simple table'>
        <TableHead>
          <TableRow>
            <TableCell>ID</TableCell>
            <TableCell align='right'>address&nbsp;set&nbsp;name</TableCell>
            <TableCell align='right'>mintKey</TableCell>
            <TableCell align='right'>minter</TableCell>
            <TableCell align='right'>admin</TableCell>
            <TableCell align='right'>artist</TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {props.addressSets.map((addressSet: IAddressSet) => (
            <TableRow
              key={addressSet.id}
              sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
            >
              <TableCell component='th' scope='row'>
                {addressSet.id}
              </TableCell>
              <TableCell align='right'>
                {shortenTezosAddress(addressSet.setName)}
              </TableCell>
              <TableCell align='right'>
                {shortenTezosAddress(addressSet.mintKey)}
              </TableCell>
              <TableCell align='right'>
                {shortenTezosAddress(addressSet.minter)}
              </TableCell>
              <TableCell align='right'>
                {shortenTezosAddress(addressSet.admin)}
              </TableCell>
              <TableCell align='right'>
                {shortenTezosAddress(addressSet.artist)}
              </TableCell>
            </TableRow>
          ))}
        </TableBody>
      </Table>
    </TableContainer>
  );
};
const formReducer = (state: any, event: any) => {
  return {
    ...state,
    [event.target.name]: event.target.value,
  };
};
export const AddressSet = () => {
  const { notify, apiService } = useApp();
  const [formData, setFormData] = useReducer(formReducer, {});
  const [addressSets, setAddressSets] = useState<IAddressSet[]>([]);
  /*
  const [setName, setSetName] = useState<string>('');
  const [mintKey, setMintKey] = useState<string>('');
  const [minter, setMinter] = useState<string>('');
  const [admin, setAdmin] = useState<string>('');
  const [artist, setArtist] = useState<string>('');
  const handleSetNameChange = (e: any) => {
    setSetName(e.target.value);
  };
  const handleMintKeyChange = (e: any) => {
    setMintKey(e.target.value);
  };
  const handleMinterChange = (e: any) => {
    setMinter(e.target.value);
  };
  const handleAdminChange = (e: any) => {
    setAdmin(e.target.value);
  };
  const handleArtistChange = (e: any) => {
    setArtist(e.target.value);
  };
*/
  const getAddressesSets = useCallback(async () => {
    const sets = await apiService.getAddressesSets();
    setAddressSets(sets);
  }, [apiService]);
  useEffect(() => {
    getAddressesSets();
  }, [getAddressesSets]);
  const handleFormSend = async (e: any) => {
    e.preventDefault();
    const data = formData;
    console.log(data);
    try {
      const addResult = await apiService.addAddressSet(data);
      notify({
        msg: `Address set ${addResult.setName} created!`,
      });
    } catch (e) {
      notify({ msg: 'Can not add address set.' });
      console.error(e);
    }
  };
  return (
    <GridContent>
      <AddAddressSetForm
        handleChange={setFormData}
        handleFormSend={handleFormSend}
      />
      <br />
      <AddressSetsList addressSets={addressSets} />
    </GridContent>
  );
};
