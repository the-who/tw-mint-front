import FormControl from '@mui/material/FormControl';
import InputLabel from '@mui/material/InputLabel';
import Paper from '@mui/material/Paper';
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import React, { useCallback, useEffect, useReducer, useState } from 'react';
import {
  IAddressSet,
  IAssetsPack,
  ICollection,
  IContract,
} from '../interfaces/apiInterfaces';
import { useApp } from '../services/app.service';
// import Select from '@mui/material/Select';

import { Grid, styled, Typography } from '@mui/material';
import { GridContent } from '../common-front/components/GridContent';
import { MyTextInput } from '../common-front/components/MyTextInput';
import { shortenTezosAddress } from '../common-front/common/utils';
import { MyButton } from '../common-front/components/MyButton';
import { MySelect } from '../common-front/components/MySelect';

export const CollectionsList = (props: { collections: ICollection[] }) => {
  return (
    <TableContainer component={Paper} color='primary.main'>
      <Table sx={{ minWidth: 650 }} aria-label='simple table'>
        <TableHead>
          <TableRow>
            <TableCell>ID</TableCell>
            <TableCell align='right'>Collection name</TableCell>
            <TableCell align='right'>Contract address</TableCell>
            <TableCell align='right'>Assets name</TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {props.collections.map((collection) => (
            <TableRow
              key={collection.id}
              sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
            >
              <TableCell component='th' scope='row'>
                {collection.id}
              </TableCell>
              <TableCell align='right'>{collection.name}</TableCell>
              <TableCell align='right'>{collection.contract.address}</TableCell>
              <TableCell align='right'>{collection?.assets?.name}</TableCell>
            </TableRow>
          ))}
        </TableBody>
      </Table>
    </TableContainer>
  );
};
const formReducer = (state: any, event: any) => {
  return {
    ...state,
    [event.name]: event.value,
  };
};

export const ContractSelector = (props: any) => {
  const { formData, handleContractSelect, contracts, contract } = props;
  return (
    <Grid>
      <FormControl fullWidth>
        <InputLabel id='contractSelect'>
          Select already deployed contract
        </InputLabel>
        <MySelect
          labelId='contractSelect'
          value={formData.contractId || ''}
          label='Select contract'
          name='contractId'
          onChange={handleContractSelect}
          items={contracts.map((contract: IDeployedContract) => {
            const { address, id, collectionName } = contract;
            return {
              name: `${id} / ${shortenTezosAddress(
                address,
              )} / ${collectionName}`,
              value: id,
            };
          })}
          defitem={{ name: 'none', value: 'none' }}
        />
      </FormControl>
      <MyButton>
        <Typography>Collection name: {contract.collectionName}</Typography>
      </MyButton>
      <br />
      <MyButton>
        <Typography>Description: {contract.description}</Typography>
      </MyButton>
      <br />
      <MyButton>
        <Typography>Authors: {contract.authors}</Typography>
      </MyButton>
    </Grid>
  );
};

export interface IDeployedContract extends IContract {
  collectionName: string;
  description: string;
  authors: string;
}

const Item = styled(Paper)(({ theme }) => ({
  ...theme.typography.body2,
  padding: theme.spacing(1),
  textAlign: 'center',
  color: theme.palette.text.disabled,
}));

export const Collections = () => {
  const { notify, apiService, tzkt } = useApp();
  const [formData, setFormData] = useReducer(formReducer, {});
  const [collections, setCollections] = useState<ICollection[]>([]);
  const [assets, setAssets] = useState<IAssetsPack[]>([]);
  const [contracts, setContracts] = useState<IDeployedContract[]>([]);
  const [contData, setContData] = useState<IDeployedContract>({
    collectionName: '',
    description: '',
    authors: '',
    address: '',
    code: '',
  });
  const [createFromDeployedContract, setCreateFromDeployedContract] =
    useState<boolean>(false);
  const [addressesSets, setAddressesSets] = useState<IAddressSet[] | null>(
    null,
  );
  const handleChange = (event: { target: { name: any; value: any } }) => {
    setFormData({
      name: event.target.name,
      value: event.target.value,
    });
  };
  const handleContractSelect = (e: any) => {
    setCreateFromDeployedContract(true);
    const id = e.target.value;
    const contract = contracts.find((c) => c.id === id);
    if (contract && contract.id) {
      handleChange({
        target: {
          value: id,
          name: e.target.name,
        },
      });
      // setContId(contract.id);
      setContData(contract);
    } else {
      console.error(contract);
      notify({
        msg: 'Something went wrong, please, try again.',
        status: 'error',
      });
    }
  };

  const getCollections = useCallback(async () => {
    const collections = await apiService.getCollections();
    setCollections(collections);
  }, [apiService, setCollections]);

  const getAddressesSets = useCallback(async () => {
    const sets = await apiService.getAddressesSets();
    setAddressesSets(sets);
  }, [apiService]);

  const getContracts = useCallback(async () => {
    const deployedContracts = await apiService.getUnusedContracts();
    const myContractsPromises = deployedContracts.map(async (contract) => {
      const address = contract.address;
      const ipfsLink = await tzkt.getCollectionMetadataIpfsLink({ address });
      const res = await fetch(ipfsLink);
      const r: { name: string; description: string; authors?: Array<string> } =
        await res.json();
      // console.log(r);
      const authors = r.authors ? r.authors : ['none'];
      const contractData = {
        id: contract.id,
        collectionName: r.name,
        description: r.description,
        authors: authors.join(','),
        address,
        code: '',
      };
      return contractData;
    });
    const myContracts = await Promise.all(myContractsPromises);
    setContracts(myContracts);
  }, [apiService, tzkt]);

  const getAssetsPacks = useCallback(async () => {
    const assetsPacks = await apiService.getAssets();
    setAssets(assetsPacks);
  }, [apiService]);

  useEffect(() => {
    getCollections();
    getAddressesSets();
    getAssetsPacks();
    getContracts();
  }, [getCollections, getAddressesSets, getAssetsPacks, getContracts]);

  const createCollectionWithAssets = async (e: any) => {
    e.preventDefault();
    if (createFromDeployedContract) {
      notify({
        msg: 'You selected already deployed contract and can not deploy new collection with the same name. Please, press "Combine collection" button or reset this form and fill it again without selecting contract',
        status: 'error',
      });
      return;
    }
    const data = formData;
    console.log(data);
    try {
      const addResult = await apiService.deployGCWithUploadedItems(data);
      notify({
        msg: `Collection ${addResult.name} created. Contract ${addResult.contract.address} deployed!`,
      });
    } catch (e) {
      notify({ msg: 'Can not deploy contract.', status: 'error' });
      console.error(e);
    }
  };
  const combineCollection = async (e: any) => {
    e.preventDefault();
    const data = formData;
    console.log(data);
    try {
      const addResult = await apiService.combineCollection(data);
      notify({
        msg: `Collection ${addResult.name} created. Contract ${addResult.contract.address} deployed!`,
      });
    } catch (e) {
      notify({ msg: 'Can not deploy contract.', status: 'error' });
      console.error(e);
    }
  };

  const mainInputs = (
    <Grid>
      <MyTextInput
        type='text'
        name='collectionName'
        placeholder='Enter collection name'
        onChange={handleChange}
        disabled={createFromDeployedContract}
        required={true}
      />
      <br />
      <MyTextInput
        name='description'
        onChange={handleChange}
        placeholder='Description for collection'
        multiline
        disabled={createFromDeployedContract}
        required
      />
      <br />
      <MyTextInput
        type='text'
        name='authors'
        placeholder='Authors, delimited by comma'
        onChange={handleChange}
        disabled={createFromDeployedContract}
        required={true}
      />
    </Grid>
  );
  return (
    <GridContent l={2} c={8} r={2}>
      <Grid
        component='form'
        item
        noValidate
        autoComplete='off'
        sx={{ width: '100%', '& > *': { pr: '2%', pl: '2%' } }}
      >
        <Grid container spacing={1}>
          <Grid item xs={6}>
            {mainInputs}
          </Grid>
          <Grid item xs={1}>
            <Item>
              <Typography sx={{ fontSize: 24 }}>OR</Typography>
            </Item>
          </Grid>
          <Grid item xs={5}>
            <ContractSelector
              handleContractSelect={handleContractSelect}
              formData={formData}
              contracts={contracts}
              contract={contData}
            />
          </Grid>
        </Grid>
        <br />
        <FormControl fullWidth>
          {/* <InputLabel id='addressSetSelect'>Select addresses set</InputLabel> */}
          <MySelect
            labelId='addressSetSelect'
            value={formData.storageAddressesId || ''}
            label='Select addresses set'
            name='storageAddressesId'
            onChange={handleChange}
            items={
              addressesSets
                ? addressesSets.map((addressSet) => {
                    const id = addressSet.id;
                    return {
                      name: `${id}_${addressSet.setName}_${addressSet.admin}`,
                      value: id,
                    };
                    // return (
                    //   <MenuItem
                    //     key={id}
                    //     value={id}
                    //   >{}</MenuItem>
                    // );
                  })
                : []
            }
          />
        </FormControl>
        <FormControl fullWidth>
          {/* <InputLabel id='assetsPackSelect'>Select assets pack</InputLabel> */}
          <MySelect
            labelId='assetsPackSelect'
            value={formData.assetsPackId || ''}
            label='Select assets'
            name='assetsPackId'
            onChange={handleChange}
            items={
              assets
                ? assets.map((assetsPack) => {
                    const id = assetsPack.id;
                    return {
                      name: `${id}_${assetsPack.id}_${assetsPack.name}`,
                      value: id,
                    };
                    // return (
                    //   <MenuItem
                    //     key={id}
                    //     value={id}
                    //   >{}</MenuItem>
                    // );
                  })
                : []
            }
          />
        </FormControl>
        <br />
        <MyButton
          // variant='contained'
          onClick={createCollectionWithAssets}
          disabled={createFromDeployedContract}
          text={`Deploy generative collection contract`}
        />
        <MyButton
          // variant='contained'
          onClick={combineCollection}
          disabled={!createFromDeployedContract}
          text={`Combine collection`}
        />
      </Grid>
      <br />
      <CollectionsList collections={collections} />
    </GridContent>
  );
};
