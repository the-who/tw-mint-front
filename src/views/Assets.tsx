import React from 'react';
import { useCallback, useEffect, useState } from 'react';
import Paper from '@mui/material/Paper';
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import Box from '@mui/material/Box';
import { IAssetsPack } from '../interfaces/apiInterfaces';
import { useApp } from '../services/app.service';
import { GridContent } from '../common-front/components/GridContent';
import { MySelect } from '../common-front/components/MySelect';
import { MyButton } from '../common-front/components/MyButton';

export const AssetsList = (props: { assets: IAssetsPack[] }) => {
  return (
    <TableContainer component={Paper}>
      <Table sx={{ minWidth: 650 }} aria-label='simple table'>
        <TableHead>
          <TableRow>
            <TableCell>ID</TableCell>
            <TableCell align='right'>Assets pack name</TableCell>
            <TableCell align='right'>thumbnail</TableCell>
            <TableCell align='right'>provenance</TableCell>
            <TableCell align='right'>bucket</TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {props.assets.map((assetsPack) => (
            <TableRow
              key={assetsPack.id}
              sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
            >
              <TableCell component='th' scope='row'>
                {assetsPack.id}
              </TableCell>
              <TableCell align='right'>{assetsPack.name}</TableCell>
              <TableCell align='right'>{assetsPack.thumbnail}</TableCell>
              <TableCell align='right'>{assetsPack.provenance}</TableCell>
              <TableCell align='right'>{assetsPack.bucket}</TableCell>
            </TableRow>
          ))}
        </TableBody>
      </Table>
    </TableContainer>
  );
};

export const Assets = () => {
  const { notify, apiService } = useApp();
  const [assets, setAssets] = useState<IAssetsPack[]>([]);
  // const [assetsName, setAssetsName] = useState<string>('');
  const [fileList, setFileList] = useState<FileList | null>(null);
  const [format, setFormat] = useState<'svg' | 'png'>('svg');
  const getAssets = useCallback(async () => {
    const result = await apiService.getAssets();
    setAssets(result);
  }, [apiService]);
  useEffect(() => {
    getAssets();
  }, [getAssets]);
  // const handleAssetsNameChange = (e: any) => {
  //   setAssetsName(e.target.value);
  // };
  const handleSelectFormat = (e: any) => {
    setFormat(e.target.value);
  };
  const handleFileSelect = (e: any) => {
    setFileList(e.target.files);
  };
  const handleUploadFiles = async (e: any) => {
    e.preventDefault();
    if (fileList === null) return;
    // console.log(fileList);
    const data = new FormData();
    const fl = fileList[0];
    data.append('file', fl);
    // data.append('assetsName', assetsName);
    data.append('format', format);
    console.log(data);
    try {
      const uploadResult = await apiService.addAssets(data);
      notify({
        msg: `Files uploaded! ${uploadResult.numberOfFiles}
        All files valid: ${uploadResult.isAllFilesValid}
        First failed file: ${uploadResult.firstNotValidEntryId}
        Assets pack ID/name: ${uploadResult.assetsInfo.id}/${uploadResult.assetsInfo.name}`,
      });
      getAssets();
    } catch (e: any) {
      notify({ msg: `Can not upload assets: ${e.message}`, status: 'error' });
      console.error(e);
    }
  };

  return (
    <GridContent>
      <Box>
        <MySelect
          labelId='formatSelect'
          value='svg'
          label='Select format'
          onChange={handleSelectFormat}
          items={[{ name: 'SVG', value: 'svg' }]}
        />
        <br />
        <section title='Select assets'>
          <h3>Please, load an archive with asset's</h3>
          <input
            type='file'
            title='files'
            name='files'
            multiple={false}
            onChange={handleFileSelect}
          />
        </section>
        <br />
        <MyButton variant='outlined' text='send' onClick={handleUploadFiles} />
      </Box>
      <hr />
      <AssetsList assets={assets} />
    </GridContent>
  );
};
