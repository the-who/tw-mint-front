import { Link } from 'react-router-dom';
import Typography from '@mui/material/Typography';
import { GridContent } from '../common-front/components/GridContent';
import { EasyStepper } from '../common-front/components/EasyStepper';

export const StepContent = (props: {
  title: string;
  description: string;
  link: string;
}) => {
  return (
    <Typography sx={{ mt: 2, mb: 1 }}>
      <Link to={props.link}>{props.title}</Link>
      <br />
      {props.description}
    </Typography>
  );
};
const allSteps = [
  {
    label: 'Address set',
    skippable: true,
    title: 'Add addresses',
    description:
      'If it is necessary, you can add new address set at "ADDRESS SETS" page ',
    link: '/addressSet',
  },
  {
    label: 'Upload assets',
    skippable: false,
    title: 'Upload new assets',
    description:
      'Create separate folder with assets(svg). Compress it. Upload.',
    link: '/assets',
  },
  {
    label: 'Combine collection',
    skippable: false,
    title: 'Combine collection from provided info',
    description: `Deploy new generative collection with added addresses and assets. Don't forget to give a good name for your collection`,
    link: '/collections',
  },
];
export const Home = () => {
  // const addressSetStep = <StepContent title='' description={} />;
  // const stepContent = <StepContent />;
  const steps = allSteps.map((s) => {
    return {
      label: s.label,
      skippable: s.skippable,
      content: (
        <StepContent
          title={s.title}
          description={s.description}
          link={s.link}
        />
      ),
    };
  });
  return (
    <GridContent>
      <EasyStepper steps={steps} />
    </GridContent>
  );
};
