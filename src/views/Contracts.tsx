import Paper from '@mui/material/Paper';
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import Stack from '@mui/material/Stack';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import { useCallback, useEffect, useReducer, useState } from 'react';
import { GridContent } from '../common-front/components/GridContent';
import { MyTextInput } from '../common-front/components/MyTextInput';
import { IContract } from '../interfaces/apiInterfaces';
import { useApp } from '../services/app.service';
import { MyButton } from '../common-front/components/MyButton';

export const AddContractForm = (props: any) => {
  return (
    <form action='POST'>
      <MyTextInput
        type='text'
        name='address'
        placeholder='Enter contract address'
        onChange={props.handleChange}
        required={true}
      />
      <br />
      <br />
      <MyButton onClick={props.handleFormSend}>Add contract</MyButton>
    </form>
  );
};
export const ContractsList = (props: { contracts: IContract[] }) => {
  return (
    <TableContainer component={Paper} color='primary.main'>
      <Table sx={{ minWidth: 650 }} aria-label='simple table'>
        <TableHead>
          <TableRow>
            <TableCell>ID</TableCell>
            <TableCell align='right'>address</TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {props.contracts.map((contract: IContract) => (
            <TableRow
              key={contract.id}
              sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
            >
              <TableCell component='th' scope='row'>
                {contract.id}
              </TableCell>
              <TableCell align='right'>{contract.address}</TableCell>
            </TableRow>
          ))}
        </TableBody>
      </Table>
    </TableContainer>
  );
};
const formReducer = (state: any, event: any) => {
  return {
    ...state,
    [event.target.name]: event.target.value,
  };
};
export const Contracts = () => {
  const { notify, apiService } = useApp();
  const [formData, setFormData] = useReducer(formReducer, {});
  const [contracts, setContracts] = useState<IContract[]>([]);
  const getContracts = useCallback(async () => {
    const createdContracts = await apiService.getContracts();
    setContracts(createdContracts);
  }, [apiService]);
  useEffect(() => {
    getContracts();
  }, [getContracts]);
  const handleFormSend = async (e: any) => {
    e.preventDefault();
    const data = formData;
    console.log(data);
    try {
      const addResult = await apiService.addContract(data);
      notify({
        msg: `Contract ${addResult.address} added!`,
      });
    } catch (e) {
      notify({ msg: 'Can not add address set.' });
      console.error(e);
    }
  };
  return (
    <GridContent>
      <Stack>
        <AddContractForm
          handleChange={setFormData}
          handleFormSend={handleFormSend}
        />
        <br />
        <ContractsList contracts={contracts} />
      </Stack>
    </GridContent>
  );
};
