import { FrontCommonConfig } from '../common-front/services/config.service';

export type Env = 'dev' | 'prod' | 'test';
export interface IConfigParams {
  env?: Env;
  appName: string;
}

export class ConfigService extends FrontCommonConfig {
  // private readonly env?: Env;
  public readonly appName: string = 'Collection generator';
  constructor(protected env: { [k: string]: string | undefined }) {
    super(env);
  }
  get tzktUrl() {
    return 'https://api.hangzhou2net.tzkt.io/v1';
  }
}
