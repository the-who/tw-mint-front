import React, { useState } from 'react';
import { TzktRest } from '../common-front/common/libs/tzkt/tzktRest';
import { ICommonApp, INotifyMessage } from '../common-front/interfaces';
import { ApiService } from './api.service';
// import { TzktService } from './tzkt.service';
// import { ConfigService } from './config.service';

// const config = new ConfigService(process.env);
const apiService = new ApiService();
const tzkt = new TzktRest({
  logger: console,
  baseUrl:
    process.env.REACT_APP_NODE_ENV === 'development'
      ? 'https://api.hangzhou2net.tzkt.io/v1'
      : 'https://api.tzkt.io/v1',
});

// export interface INotifyMessage {
//   msg: string;
//   status?: 'success' | 'error';
// }

export interface IApp extends ICommonApp {
  notify: (params: INotifyMessage) => void;
  apiService: ApiService;
  messages: INotifyMessage[];
  setMessages: (a: any) => any;
  tzkt: TzktRest;
}

const AppContext = React.createContext<any>(undefined);

export const AppProvider: React.FC = ({ children }) => {
  // const [userAddress, setUserAddress] = useState<string | null>(null);
  // const [userBalance, setUserBalance] = useState<number | null>(null);
  // const [buying, setBuying] = useState<boolean>(false);
  // const [connectingWallet, setConnectingWallet] = useState<boolean>(false);
  const [messages, setMessages] = useState<INotifyMessage[]>([]);
  const notify = (params: INotifyMessage): any => {
    if (messages.length > 5) messages.shift();
    setMessages([...messages, params]);
  };
  let v = { notify, apiService, messages, setMessages, tzkt };
  return <AppContext.Provider value={v}>{children}</AppContext.Provider>;
};

export const useApp: () => IApp = () => React.useContext(AppContext);
