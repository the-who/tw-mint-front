import {
  IAddAssetsResult,
  IAddressSet,
  IAssetsPack,
  ICollection,
  IContract,
  ICreateNewCollectionWithAssetsResult,
} from '../interfaces/apiInterfaces';

export interface IHttpError {
  statusCode: number;
}

export class ApiService {
  private readonly baseUrl = process.env.REACT_APP_API_BASE_URL;
  private async get(path: string, params = {}) {
    console.log(this.baseUrl);
    const fetchUrl = `${this.baseUrl}/${path}`;
    const r = await fetch(fetchUrl);
    return await r.json();
  }
  private async post(path: string, data: any, params = {}, withFile = false) {
    const headers = withFile
      ? undefined
      : {
          'Content-Type': 'application/json',
        };
    const p: RequestInit = {
      method: 'POST',
      body: !withFile ? JSON.stringify(data) : data,
      headers,
      ...params,
    };
    const postUrl = `${this.baseUrl}/${path}`;
    const r = await fetch(postUrl, p);
    const result = await r.json();
    if (result.statusCode === 400) {
      throw new Error(`${result.message}`);
    }
    return result;
  }
  async getAddressesSets(): Promise<IAddressSet[]> {
    const path = 'addresses';
    const result = await this.get(path);
    if (Array.isArray(result)) return result;
    else return [];
  }
  async getContracts(): Promise<IContract[]> {
    const path = 'contracts';
    const result = await this.get(path);
    if (Array.isArray(result)) return result;
    else return [];
  }
  async getUnusedContracts(): Promise<IContract[]> {
    const path = 'unusedContracts';
    const result = await this.get(path);
    console.log(result);
    if (Array.isArray(result)) return result;
    else return [];
  }
  async getCollections(): Promise<ICollection[]> {
    const path = 'collections';
    const result = await this.get(path);
    if (Array.isArray(result)) return result;
    else return [];
  }
  async getAssets(): Promise<IAssetsPack[]> {
    const path = 'unusedAssets';
    const result = await this.get(path);
    if (Array.isArray(result)) return result;
    else return [];
  }
  async createNewCollectionWithAssets(
    collectionData: any,
  ): Promise<ICreateNewCollectionWithAssetsResult> {
    const path = 'createCollectionWithAsstes';
    return await this.post(path, collectionData, {}, true);
  }
  async addAddressSet(addressSet: any): Promise<IAddressSet> {
    const path = 'addAddressSet';
    return await this.post(path, addressSet);
  }
  async addAssets(assetsData: any): Promise<IAddAssetsResult> {
    const path = 'addAssets';
    return await this.post(path, assetsData, {}, true);
  }
  async addContract(contractData: any): Promise<IContract> {
    const path = 'addContract';
    return await this.post(path, contractData);
  }
  async deployGCWithUploadedItems(collectionData: any): Promise<ICollection> {
    const path = 'deployGCWithUploadedItems';
    return await this.post(path, collectionData);
  }
  async combineCollection(collectionData: any): Promise<ICollection> {
    const path = 'combineCollection';
    return await this.post(path, collectionData);
  }
}
