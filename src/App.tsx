import { Routes, Route } from 'react-router-dom';
// import { Header } from './views/Header';
import { Footer } from './views/Footer';
import './App.css';
import { AddressSet } from './views/AddressSet';
import { Assets } from './views/Assets';
import { useApp } from './services/app.service';
import { Collections } from './views/Collections';
import { Contracts } from './views/Contracts';
import { Grid } from '@mui/material';
import { Home } from './views/Home';
import { Header } from './common-front/components/MyHeader';
import { Router } from './common-front/theme';
import { MyBackdrop } from './common-front/components/BackDrop';
import { AlertBox } from './common-front/components/AlertBox';
import { IHeaderButtonData } from './common-front/interfaces';

function App() {
  const { messages, setMessages } = useApp();
  const buttons: IHeaderButtonData[] = [
    {
      name: 'Home',
      href: '',
    },
    {
      name: 'Address sets',
      href: 'addressSet',
    },
    {
      name: 'assets',
      href: 'assets',
    },
    {
      name: 'collections',
      href: 'collections',
    },
    {
      name: 'contracts',
      href: 'contracts',
    },
  ];
  const onAlertClose = (id: any) => {
    const nm = messages.filter((m, i) => i !== id);
    setMessages(nm);
  };
  return (
    <Grid container className='App' sx={{ flexGrow: 1 }}>
      <Router>
        <Header buttons={buttons} loading={false} isDapp={false} />
        <Grid item xs={12}>
          <AlertBox messages={messages} onClose={onAlertClose} />
          <Routes>
            <Route path='/' element={<Home />} />
            <Route path='/addressSet' element={<AddressSet />} />
            <Route path='/assets' element={<Assets />} />
            <Route path='/collections' element={<Collections />} />
            <Route path='/contracts' element={<Contracts />} />
          </Routes>
        </Grid>
        <Footer />
      </Router>
      <MyBackdrop open={false} />
    </Grid>
  );
}

export default App;
