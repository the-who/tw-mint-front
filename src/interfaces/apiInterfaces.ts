export interface ICreateNewCollectionWithAssetsResult {
  isAllFilesValid: boolean;
  numberOfFiles?: number;
  firstNotValidEntryId?: string;
  s3uploadResult: any;
  ipfsUploadResult: any;
  contractAddress: string;
}
export interface IAddAssetsResult {
  isAllFilesValid: boolean;
  numberOfFiles?: number;
  firstNotValidEntryId?: string;
  s3uploadResult: any;
  ipfsUploadResult: any;
  assetsInfo: IAssetsPack;
}
export interface IAssetsPack {
  id?: number;
  name: string;
  thumbnail: string;
  provenance: string;
  bucket: string;
}
export interface IAddressSet {
  id: number;
  setName: string;
  mintKey: string;
  admin: string;
  minter: string;
  artist: string;
}

export interface IContract {
  id?: number;
  address: string;
  code?: string;
}

export interface ICollection {
  id: number;
  name: string;
  description: string;
  authors: string;
  contract: IContract;
  addresses?: IAddressSet;
  assets?: IAssetsPack;
  soldout?: boolean;
}
