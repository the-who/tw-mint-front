# Stage 1
FROM node:16 as build-stage

WORKDIR /app
COPY package.json .
COPY yarn.lock .
RUN yarn install
COPY src ./src
COPY public ./public

ARG API_BASE_URL
ENV REACT_APP_API_BASE_URL=$API_BASE_URL

ARG MY_ENV
ENV REACT_APP_NODE_ENV=$MY_ENV

RUN yarn run build

# Stage 2
FROM nginx:latest

COPY --from=build-stage /app/build /etc/nginx/html
COPY ./nginx.conf /etc/nginx/conf.d/default.conf
EXPOSE $REACT_DOCKER_PORT
EXPOSE 8080

CMD nginx -g 'daemon off;'